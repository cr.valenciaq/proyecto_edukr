from django.contrib import admin
from .models import Genero, Materia

# Register your models here.

class MateriaAdmin(admin.ModelAdmin):
    list_display =['nombre', 'duracion', 'genero']
  


admin.site.register(Genero)
admin.site.register(Materia, MateriaAdmin)