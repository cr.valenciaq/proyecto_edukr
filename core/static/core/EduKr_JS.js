/**Header Dinámico**/
$(document).ready(function(){

    $(window).scroll(function(){
        if ( $(this).scrollTop()>0){
            $('header').addClass('header2');
        } else {
            $('header').removeClass('header2');
        }
    });
});

/**Solo Texto**/
jQuery(document).ready(function() {
    jQuery('#nombre').keypress(function(tecla) {
        if((tecla.charCode < 97 || tecla.charCode > 122) && 
            (tecla.charCode < 65 || tecla.charCode > 90) && 
            (tecla.charCode != 45)) 
        return false;
    });
});

/**Validación Formulario**/
var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

$(document).ready(function(){
    $("#form-boton").click(function(){
        var nombre = $("#nombre").val();
        var direccion = $("#direccion").val();
        var correo = $("#correo").val();
        var user = $("#user").val();
        var pass = $("#pass").val();
        var lista = $("input[type = 'radio']:checked");

        if(nombre == "" || nombre.length <= 15){
            $("#mensaje1").fadeIn();
            return false;
        } else {
            $("#mensaje1").fadeOut();
            if(direccion == "" || direccion.length <= 10){
                $("#mensaje2").fadeIn();
                return false;
            } else {
                $("#mensaje2").fadeOut();
                if(correo == "" || !expr.test(correo)){
                    $("#mensaje3").fadeIn();
                    return false;
                } else {
                    $("#mensaje3").fadeOut();
                    if(user == "" || user.length <= 15 ){
                        $("#mensaje4").fadeIn();
                        return false;
                    } else {
                        $("#mensaje4").fadeOut();
                        if(pass == "" || pass.length < 9 ){
                            $("#mensaje5").fadeIn();
                            return false;
                        } else {
                            $("#mensaje5").fadeOut();
                            if(lista.length == 0){
                                $("#mensaje6").fadeIn();
                                return false;
                            } else {
                                $("#mensaje6").fadeOut();
                                alert("!Registro Exitoso¡");
                            };
                            
                        }
                    }
                }
            }
        }
    });
});

$(function(){
    $('#galeria a').lightBox();
});