from django.db import models

# Create your models here.

class Genero(models.Model):
    nombre =models.CharField(max_length=80)

    def __str__(self):
        return self.nombre

class Materia(models.Model):
    nombre =models.CharField(max_length=200)
    duracion =models.IntegerField()
    genero =models.ForeignKey(Genero, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre